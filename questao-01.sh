#!/bin/bash

read -p "digite o primeiro numero: " a
echo

echo "Adição(+), Subtração(-), Multiplicação(*), Divisão(/), Raiz quadrada(//)"
read -p "digite a operação desejada +, -, *, /, //: " op
echo

echo -e "\nPara raiz quadrada (//) digite 0 no segundo numero"
read -p "digite o segundo numero: " b

raiz=$(echo "scale=2; sqrt($a)" | bc)
resultado=$(python3 -c "print($a $op $b)" 2>>/dev/null )

[ $op == "//" ] && echo $raiz 2>>/dev/null || echo $resultado 2>>/dev/null

