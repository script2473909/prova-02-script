#!/bin/bash

data=$(date -d "next wed" +%d-%m-%Y) ; data2=$(date -d "next wed +7 days" +%d-%m-%Y) ; data3=$(date -d "next wed +14 days" +%d-%m-%Y) ; data4=$(date -d "next wed +21 days" +%d-%m-%Y)

echo -e "\n$data\n$data2\n$data3\n$data4\n"

dir=backup-$data ; dir2=backup-$data2 ; dir3=backup-$data3 ; dir4=backup-$data4

mkdir "$dir" "$dir2" "$dir3" "$dir4" && ls -l ./
