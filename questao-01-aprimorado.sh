#!/bin/bash

read -p "digite o primeiro numero: " a
echo

echo "Adição(+), Subtração(-), Multiplicação(*), Divisão(/), Raiz quadrada(//)"
read -p "digite a operação desejada +, -, *, /, //: " op
echo

2>>/dev/null
raiz=$(echo "scale=2; sqrt($a)" | bc)

[ $op == "//" ] && echo $raiz && exit || read -p "digite o segundo numero: " b && echo $(python3 -c "print($a $op $b)")
 
2>>/dev/null
